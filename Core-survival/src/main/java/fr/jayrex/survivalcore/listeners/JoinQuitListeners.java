package fr.jayrex.survivalcore.listeners;

import fr.jayrex.survivalcore.SurvivalCorePlugin;
import fr.jayrex.survivalcore.utils.Scheduler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinQuitListeners implements Listener {

    private final SurvivalCorePlugin plugin;

    public JoinQuitListeners(SurvivalCorePlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        player.setInvulnerable(true);

        Scheduler.runLater(() -> player.setInvulnerable(false), 200);
    }
}
