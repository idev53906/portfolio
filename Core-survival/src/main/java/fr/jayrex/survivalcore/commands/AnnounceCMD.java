package fr.jayrex.survivalcore.commands;

import fr.jayrex.survivalcore.SurvivalCorePlugin;
import fr.jayrex.survivalcore.manager.AnnounceManager;
import org.bukkit.entity.Player;
import revxrsal.commands.annotation.Command;

public class AnnounceCMD {

    private SurvivalCorePlugin plugin;

    public AnnounceCMD(SurvivalCorePlugin plugin) {
        this.plugin = plugin;
    }

    @Command("annonce")
    public void sendAnnounce(Player player, String message) {
        AnnounceManager announceManager = plugin.getAnnounceManager();
        announceManager.sendAnnounce(player, message);
    }

}
