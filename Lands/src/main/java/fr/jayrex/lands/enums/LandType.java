package fr.jayrex.lands.enums;

public enum LandType {
    PLAYER,
    SYSTEM,
    SUBLAND,
    GUILD,
}
