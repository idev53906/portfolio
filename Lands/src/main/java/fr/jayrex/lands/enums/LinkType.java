package fr.jayrex.lands.enums;

public enum LinkType {
    TRUSTS,
    GLOBALTRUST,
    BANS
}
