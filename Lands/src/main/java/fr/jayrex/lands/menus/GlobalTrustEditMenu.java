package fr.jayrex.lands.menus;

import fr.jayrex.bukkitcore.menu.Menu;
import fr.jayrex.lands.LandsPlugin;
import fr.jayrex.lands.api.LandRepository;
import fr.jayrex.lands.enums.Action;
import fr.jayrex.lands.enums.ActionGroup;
import fr.jayrex.lands.model.land.Land;
import org.bukkit.entity.Player;

public class GlobalTrustEditMenu extends TrustEditMenu {

    private final LandRepository landRepository;

    public GlobalTrustEditMenu(Player player, Land land, LandsPlugin plugin, Menu previousMenu, ActionGroup actionGroup) {
        super(player, land, plugin, previousMenu, actionGroup);
        this.landRepository = plugin.getLandRepository();
        this.trust = land.getGlobalTrust();
    }

    @Override
    public String getMenuName() {
        return "§2Permissions globales";
    }

    @Override
    protected void addTrust(Action action) {
        landRepository.addGlobalTrust(land, action);
        trust.addPermission(action);
    }

    @Override
    protected void removeTrust(Action action) {
        landRepository.removeGlobalTrust(land, action);
        trust.removePermission(action);
    }
}
