package fr.jayrex.lands.listeners;

import fr.jayrex.lands.LandsPlugin;
import fr.jayrex.lands.api.LandRepository;
import fr.jayrex.lands.enums.Action;
import fr.jayrex.lands.model.land.Land;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTakeLecternBookEvent;

public class PlayerTakeLecternBookListener implements Listener {

    private final LandRepository landRepository;

    public PlayerTakeLecternBookListener(LandsPlugin plugin) {
        this.landRepository = plugin.getLandRepository();
    }

    @EventHandler
    public void onTakeBook(PlayerTakeLecternBookEvent event) {
        Player player = event.getPlayer();
        Land land = landRepository.getLandAt(event.getLectern().getLocation());

        if (!land.isBypassing(player, Action.LECTERN_TAKE)) {
            event.setCancelled(true);
        }
    }
}
