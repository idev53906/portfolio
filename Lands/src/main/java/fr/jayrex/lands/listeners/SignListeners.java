package fr.jayrex.lands.listeners;

import fr.jayrex.lands.LandsPlugin;
import fr.jayrex.lands.api.LandRepository;
import fr.jayrex.lands.enums.Action;
import fr.jayrex.lands.model.land.Land;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class SignListeners implements Listener {

    private final LandRepository landRepository;

    public SignListeners(LandsPlugin plugin) {
        this.landRepository = plugin.getLandRepository();
    }

    @EventHandler
    public void onSignChange(SignChangeEvent event) {
        Player player = event.getPlayer();
        Land land = landRepository.getLandAt(event.getBlock().getLocation());
        if (land != null && !land.isBypassing(player, Action.SIGN_EDIT)) {
            event.setCancelled(true);
        }
    }
}
