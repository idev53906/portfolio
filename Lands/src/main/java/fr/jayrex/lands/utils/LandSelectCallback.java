package fr.jayrex.lands.utils;

import fr.jayrex.lands.model.land.Land;

public interface LandSelectCallback {

    void select(Land land);

    void cancel();
}
