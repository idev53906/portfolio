package fr.jayrex.shop.listeners;

import fr.jayrex.bukkitcore.event.CoreMessageEvent;
import fr.jayrex.common.messaging.Message;
import fr.jayrex.shop.ShopItem;
import fr.jayrex.shop.ShopPlugin;
import fr.jayrex.shop.manager.ShopManager;
import fr.jayrex.shop.utils.StockSyncMessage;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SyncMessageListener implements Listener {

    private final ShopPlugin plugin;
    private final ShopManager shopManager;

    public SyncMessageListener(ShopPlugin plugin) {
        this.plugin = plugin;
        this.shopManager = plugin.getShopManager();
    }

    @EventHandler
    public void onCoreMessage(CoreMessageEvent e) {
        Message message = e.getMessage();

        if(message.getChannel().equals(ShopPlugin.STOCK_SYNC_CHANNEL)){
            StockSyncMessage stockSyncMessage = message.getMessage(StockSyncMessage.class);
            ShopItem shopItem = shopManager.getShopItemByID(stockSyncMessage.getShopID());
            shopItem.setStock(stockSyncMessage.getNewStock());
        }

    }


}
