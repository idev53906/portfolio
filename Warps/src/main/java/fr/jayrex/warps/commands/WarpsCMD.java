package fr.jayrex.warps.commands;

import fr.jayrex.warps.WarpsManager;
import fr.jayrex.warps.WarpsPlugin;
import fr.jayrex.warps.menu.MainWarpsMenu;
import fr.jayrex.warps.objects.PlayerWarp;
import fr.jayrex.warps.utils.SortingTime;
import org.bukkit.entity.Player;
import revxrsal.commands.annotation.Command;

import java.util.stream.Collectors;

public class WarpsCMD {

    private final WarpsManager manager;

    public WarpsCMD(WarpsPlugin plugin) {
        this.manager = plugin.getWarpManager();
    }

    @Command("warps")
    public void warps(Player player) {
        new MainWarpsMenu(player, manager, manager.getPlayerWarps().values().stream()
                .filter(PlayerWarp::isOpened)
                .collect(Collectors.toList()), SortingTime.ALL).open();
    }
}
