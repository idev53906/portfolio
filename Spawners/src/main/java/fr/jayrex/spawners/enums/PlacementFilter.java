package fr.jayrex.spawners.enums;

public enum PlacementFilter {
    ALL,
    PLACED,
    UNPLACED
}
