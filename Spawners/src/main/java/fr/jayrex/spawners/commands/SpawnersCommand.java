package fr.jayrex.spawners.commands;

import fr.jayrex.spawners.SpawnerManager;
import fr.jayrex.spawners.SpawnersPlugin;
import fr.jayrex.spawners.menu.SpawnerMenuData;
import fr.jayrex.spawners.menu.SpawnersMenu;
import fr.jayrex.spawners.utils.PermissionUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import revxrsal.commands.annotation.*;
import revxrsal.commands.bukkit.BukkitCommandActor;
import revxrsal.commands.bukkit.annotation.CommandPermission;

import java.util.UUID;

@Command("spawners")
public class SpawnersCommand {

    private final SpawnersPlugin plugin;
    private final SpawnerManager spawnerManager;

    public SpawnersCommand(SpawnersPlugin plugin) {
        this.plugin = plugin;
        this.spawnerManager = plugin.getSpawnerManager();
    }

    @Command("spawners")
    @DefaultFor("spawners")
    public void spawners(Player player) {
        spawnersShow(player, player);
    }

    @Subcommand("show")
    @CommandPermission("spawners.admin")
    public void spawnersShow(Player sender, OfflinePlayer target) {
        spawnerManager.getSpawnersAsync(target.getUniqueId()).thenAccept(spawners -> {
            Bukkit.getScheduler().runTask(SpawnersPlugin.getInstance(),
                    () -> new SpawnersMenu(sender, spawners, spawnerManager, target).open());
        });
    }

    @Subcommand("give")
    @CommandPermission("spawners.admin")
    public void give(BukkitCommandActor sender, OfflinePlayer target, EntityType type, @Range(min = 1) int amount) {
        for (int i = 0; i < amount; i++) {
            spawnerManager.saveSpawnerAsync(target.getUniqueId(), type);
        }
        sender.reply("§aVous avez donné " + amount + " spawner(s) à " + target.getName());
        if (target.isOnline()) {
            ((Player) target).sendMessage("§aVous avez reçu " + amount + " spawner(s) à " + type.toString().toLowerCase());
        }
    }

    @Subcommand("givepickaxe")
    @CommandPermission("spawners.admin")
    public void givePickaxe(BukkitCommandActor sender, @Range(min = 1) int durability, @Default("me") Player target) {
        if (target.getInventory().firstEmpty() != -1) {
            target.getInventory().addItem(spawnerManager.getPioche(durability));
        } else {
            target.getWorld().dropItem(target.getLocation(), spawnerManager.getPioche(durability));
        }
    }

    @Subcommand("menu")
    @CommandPermission("spawners.admin")
    public void menu(Player player) {
        UUID uuid = player.getUniqueId();

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            var spawners = spawnerManager.getSpawners(uuid);
            int spawnerLimit = PermissionUtils.getSpawnerLimitFromLP(uuid);
            spawnerManager.cacheSpawnerCount(uuid).join();

            SpawnerMenuData data = new SpawnerMenuData(player, spawners, spawnerLimit);
            Bukkit.getScheduler().runTask(plugin, () -> plugin.openSpawnerMenu(player, data));
        });
    }

}
