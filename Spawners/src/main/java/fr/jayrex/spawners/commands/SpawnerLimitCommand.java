package fr.jayrex.spawners.commands;

import fr.jayrex.spawners.utils.PermissionUtils;
import org.bukkit.entity.Player;
import revxrsal.commands.annotation.Command;
import revxrsal.commands.annotation.DefaultFor;
import revxrsal.commands.annotation.Subcommand;
import revxrsal.commands.bukkit.annotation.CommandPermission;

@Command("spawnerlimit")
public class SpawnerLimitCommand {

    @Subcommand("show")
    @DefaultFor("spawnerlimit")
    @CommandPermission("spawners.admin")
    public void showLimit(Player sender, Player target) {
        int limit = PermissionUtils.getSpawnerLimit(target);
        sender.sendMessage("§aLa limite de spawners de " + target.getName() + " est de " + limit + ".");
    }

    @Subcommand("increase")
    @CommandPermission("spawners.admin")
    public void increaseLimit(Player sender, Player target, int amount) {
        PermissionUtils.increaseSpawnerLimit(target, amount);
        sender.sendMessage("§aLa limite de spawners de " + target.getName() + " a été augmentée de " + amount + ".");
    }

    @Subcommand("decrease")
    @CommandPermission("spawners.admin")
    public void decreaseLimit(Player sender, Player target, int amount) {
        PermissionUtils.decreaseSpawnerLimit(target, amount);
        sender.sendMessage("§aLa limite de spawners de " + target.getName() + " a été diminuée de " + amount + ".");
    }

}
