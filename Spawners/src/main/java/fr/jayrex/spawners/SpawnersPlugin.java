package fr.jayrex.spawners;

import fr.jayrex.bukkitcore.CoreBukkitPlugin;
import fr.jayrex.spawners.commands.SpawnerLimitCommand;
import fr.jayrex.spawners.commands.SpawnersCommand;
import fr.jayrex.spawners.listeners.JoinQuitListeners;
import fr.jayrex.spawners.listeners.PlaceBreakListeners;
import fr.jayrex.spawners.menu.SpawnerFilterButton;
import fr.jayrex.spawners.menu.SpawnerLimitInfoButton;
import fr.jayrex.spawners.menu.SpawnerMenuData;
import fr.jayrex.spawners.menu.SpawnersButton;
import fr.maxlego08.menu.api.ButtonManager;
import fr.maxlego08.menu.api.Inventory;
import fr.maxlego08.menu.api.InventoryManager;
import fr.maxlego08.menu.button.loader.NoneLoader;
import fr.maxlego08.menu.exceptions.InventoryException;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import revxrsal.commands.bukkit.BukkitCommandHandler;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class SpawnersPlugin extends JavaPlugin {

    private static SpawnersPlugin instance;
    private SpawnerManager spawnerManager;
    private Map<UUID, SpawnerMenuData> spawnerMenuData;
    private InventoryManager inventoryManager;
    private Inventory spawnerMenu;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        instance = this;

        spawnerManager = new SpawnerManager(this);

        spawnerMenuData = new HashMap<>();

        PluginManager pm = getServer().getPluginManager();

        pm.registerEvents(new PlaceBreakListeners(this), this);
        pm.registerEvents(new JoinQuitListeners(spawnerManager), this);

        registerCommands();
        loadZMenu();
    }

    private void registerCommands() {
        BukkitCommandHandler commandHandler = BukkitCommandHandler.create(this);
        commandHandler.accept(CoreBukkitPlugin.getInstance().getCommandHandlerVisitor());

        commandHandler.getAutoCompleter().registerParameterSuggestions(EntityType.class, (args, sender, command)
                -> Arrays.stream(EntityType.values()).map(entityType -> entityType.getKey().getKey()).toList());

        commandHandler.register(new SpawnersCommand(this));
        commandHandler.register(new SpawnerLimitCommand());
        commandHandler.registerBrigadier();
    }

    public static SpawnersPlugin getInstance() {
        return instance;
    }

    public SpawnerManager getSpawnerManager() {
        return spawnerManager;
    }

    public boolean isMultipaperSupportEnabled() {
        return getConfig().getBoolean("multipaper-support.enabled", false);
    }

    public @Nullable String getMultipaperServerName() {
        return isMultipaperSupportEnabled() ? getConfig().getString("multipaper-support.server-name") : null;
    }

    public String getServerName() {
        String multipaperServer = getMultipaperServerName();
        return multipaperServer != null ? multipaperServer : CoreBukkitPlugin.getInstance().getServerName();
    }

    public SpawnerMenuData getSpawnerMenuData(Player player) {
        return spawnerMenuData.get(player.getUniqueId());
    }

    private void loadZMenu() {
        inventoryManager = getProvider(InventoryManager.class);
        ButtonManager buttonManager = getProvider(ButtonManager.class);

        if (inventoryManager == null || buttonManager == null) {
            getLogger().warning("ZMenu is not installed !");
            return;
        }

        buttonManager.register(new NoneLoader(this, SpawnersButton.class, "spawners_pagination"));
        buttonManager.register(new NoneLoader(this, SpawnerLimitInfoButton.class, "spawners_limit_info"));
        buttonManager.register(new NoneLoader(this, SpawnerFilterButton.class, "spawners_filter"));

        try {
            spawnerMenu = inventoryManager.loadInventoryOrSaveResource(this, "inventories/menu.yml");
        } catch (InventoryException exception) {
            exception.printStackTrace();
        }
    }

    private <T> @Nullable T getProvider(Class<T> classProvider) {
        RegisteredServiceProvider<T> provider = Bukkit.getServer().getServicesManager().getRegistration(classProvider);
        return provider == null ? null : provider.getProvider();
    }

    public void openSpawnerMenu(@NotNull Player player, SpawnerMenuData data) {
        spawnerMenuData.put(player.getUniqueId(), data);
        this.inventoryManager.openInventory(player, spawnerMenu);
    }
}
