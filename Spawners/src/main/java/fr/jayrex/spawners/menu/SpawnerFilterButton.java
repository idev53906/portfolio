package fr.jayrex.spawners.menu;

import fr.jayrex.spawners.SpawnersPlugin;
import fr.jayrex.spawners.enums.PlacementFilter;
import fr.maxlego08.menu.api.utils.Placeholders;
import fr.maxlego08.menu.button.ZButton;
import fr.maxlego08.menu.inventory.inventories.InventoryDefault;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class SpawnerFilterButton extends ZButton {

    private final SpawnersPlugin plugin;

    public SpawnerFilterButton(Plugin plugin) {
        this.plugin = (SpawnersPlugin) plugin;
    }

    @Override
    public ItemStack getCustomItemStack(Player player) {
        SpawnerMenuData spawnerMenuData = plugin.getSpawnerMenuData(player);
        ItemStack item = this.getItemStack().build(player, false);
        ItemMeta meta = item.getItemMeta();
        List<String> lore = meta.getLore();

        if (lore == null) return item;

        for (int i = 0; i < lore.size(); i++) {
            String line = lore.get(i);
            for (PlacementFilter value : PlacementFilter.values()) {
                String tag = "[" + value + "]";
                if (line.contains(tag)) {
                    line = (spawnerMenuData.getFilter() == value) ? line.replace(tag, "§a") : line.replace(tag, "");
                    break;
                }
            }
            lore.set(i, line);
        }

        meta.setLore(lore);
        item.setItemMeta(meta);

        return item;
    }

    @Override
    public void onClick(Player player, InventoryClickEvent event, InventoryDefault inventory, int slot, Placeholders placeholders) {
        SpawnerMenuData spawnerMenuData = plugin.getSpawnerMenuData(player);
        changeFilter(spawnerMenuData);
        plugin.openSpawnerMenu(player, spawnerMenuData);
    }

    private void changeFilter(SpawnerMenuData spawnerMenuData) {
        PlacementFilter filter = spawnerMenuData.getFilter();

        if (filter == PlacementFilter.ALL) {
            spawnerMenuData.setFilter(PlacementFilter.PLACED);
        } else if (filter == PlacementFilter.PLACED) {
            spawnerMenuData.setFilter(PlacementFilter.UNPLACED);
        } else {
            spawnerMenuData.setFilter(PlacementFilter.ALL);
        }
    }
}
