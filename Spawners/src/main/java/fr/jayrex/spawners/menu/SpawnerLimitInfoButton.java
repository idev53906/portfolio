package fr.jayrex.spawners.menu;

import fr.jayrex.spawners.SpawnerManager;
import fr.jayrex.spawners.SpawnersPlugin;
import fr.maxlego08.menu.api.utils.Placeholders;
import fr.maxlego08.menu.button.ZButton;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class SpawnerLimitInfoButton extends ZButton {

    private final SpawnersPlugin plugin;
    private final SpawnerManager spawnerManager;

    public SpawnerLimitInfoButton(Plugin plugin) {
        this.plugin = (SpawnersPlugin) plugin;
        this.spawnerManager = this.plugin.getSpawnerManager();
    }

    @Override
    public ItemStack getCustomItemStack(Player player) {
        SpawnerMenuData spawnerMenuData = plugin.getSpawnerMenuData(player);

        Placeholders placeholders = new Placeholders();
        placeholders.register("placed_count", spawnerManager.getSpawnerCount(spawnerMenuData.getTarget().getUniqueId()) + "");
        placeholders.register("placed_limit", spawnerMenuData.getSpawnerLimit() + "");
        return this.getItemStack().build(player, false, placeholders);
    }
}
