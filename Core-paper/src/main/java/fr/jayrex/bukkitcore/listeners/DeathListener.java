package fr.jayrex.bukkitcore.listeners;

import fr.jayrex.bukkitcore.CoreBukkitPlugin;
import fr.jayrex.common.messaging.message.PlayerSLocationMessage;
import fr.jayrex.common.teleport.SLocation;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class DeathListener implements Listener {
	
	private CoreBukkitPlugin plugin;
	
	public DeathListener(CoreBukkitPlugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		Location loc = player.getLocation();
		
		if(plugin.getServerName() == null) {
			return;
		}
		plugin.getMessagingManager().sendMessage("DeathLocation", new PlayerSLocationMessage(e.getEntity().getUniqueId(), new SLocation(plugin.getServerName(), loc.getWorld().getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getPitch(), loc.getYaw())));
	}

}
