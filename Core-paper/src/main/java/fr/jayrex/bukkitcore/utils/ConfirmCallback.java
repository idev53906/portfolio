package fr.jayrex.bukkitcore.utils;

public interface ConfirmCallback {
	
	void call(boolean confirmed);

}
