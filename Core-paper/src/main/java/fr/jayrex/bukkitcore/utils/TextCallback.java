package fr.jayrex.bukkitcore.utils;

public interface TextCallback {
	
	public void call(String text);

}
