package fr.jayrex.bukkitcore.manager;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import fr.jayrex.bukkitcore.CoreBukkitPlugin;
import fr.jayrex.bukkitcore.utils.Scheduler;
import fr.jayrex.common.data.Account;
import fr.jayrex.common.data.AccountDAO;
import fr.jayrex.common.messaging.CoreChannel;

import java.util.UUID;

public class AccountManager {

    private final CoreBukkitPlugin plugin;
    private final LoadingCache<UUID, Account> accounts = Caffeine.newBuilder().build(uuid -> new AccountDAO().getAccount(uuid));

    public AccountManager(CoreBukkitPlugin plugin) {
        this.plugin = plugin;
    }

    public Account getAccount(UUID uuid) {
        return accounts.get(uuid);
    }

    public void reloadAccount(UUID uuid) {
        accounts.invalidate(uuid);
        Scheduler.runAsync(() -> accounts.get(uuid));
    }

    public void saveAccount(Account account) {
        AccountDAO accountDAO = new AccountDAO();
        accountDAO.sendAccountToDB(account);
        plugin.getMessagingManager().sendMessage(CoreChannel.SYNC_ACCOUNT_CHANNEL, account.getUUID().toString());
    }

    public void saveAccountAsync(Account account) {
        Scheduler.runAsync(() -> saveAccount(account));
    }
}
