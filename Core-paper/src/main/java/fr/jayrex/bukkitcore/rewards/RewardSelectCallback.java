package fr.jayrex.bukkitcore.rewards;

public interface RewardSelectCallback {
	
	void select(Reward reward);

}
